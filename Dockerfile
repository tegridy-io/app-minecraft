FROM ubuntu:20.04 AS baseimage


RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
    apt-get update -y && \
    apt-get install -y openjdk-16-jdk-headless openjdk-16-jre-headless curl git unzip


## build minecraft using a seperate image
FROM baseimage AS buildimage

ARG REV=latest

RUN mkdir -p /build/plugins
    
RUN cd /build && \
    curl https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar -o /build/BuildTools.jar && \
    HOME=/build java -Xmx4g -Xms4g -jar BuildTools.jar --rev $REV

## final usable image
FROM baseimage AS finalimage

ENV JVM_OPTS="-Xmx4g -Xms4g"

EXPOSE 25565

RUN mkdir -p /minecraft && \
    mkdir -p /minecraft_orig && \
    mkdir -p /minecraft_run

COPY --from=buildimage /build/Spigot/Spigot-Server/target/spigot-*.jar /minecraft_orig/spigot.jar

COPY ./scripts .

CMD ["/init.sh"]
