#!/bin/bash

IMAGE_NAME=registry.gitlab.com/tegridy.io/app-minecraft
BUILD_REV="latest"

if [[ -n $1 ]]; then
  BUILD_REV=$1
fi

docker build --build-arg REV=$BUILD_REV -t $IMAGE_NAME:$BUILD_REV .
