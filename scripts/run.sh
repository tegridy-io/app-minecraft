#!/bin/bash

cmd=$@

echo $$ > /minecraft_run/minecraft.pid
rm -f /minecraft_run/$$.input
mkfifo /minecraft_run/$$.input

echo "Starting $cmd with pid $$"
exec $cmd < <(tail -f /minecraft_run/$$.input)
